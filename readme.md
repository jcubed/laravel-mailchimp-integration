Mailchimp Laravel Integration as Controller


Include:
use \App\Http\Controllers\MailchimpController;

How to use:

MailchimpController::mailchimp_add($user->email, $user->lang, $user->name, $user->type);

To edit a user to go from one list to another:

MailchimpController::mailchimp_edit($user->email, $user->lang, $newLanguage);