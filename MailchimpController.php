<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use App\User;
use App\Http\Requests;

class MailchimpController extends Controller
{

    public function mailchimp_add($email, $lang, $username, $type) {
        
    $email = $email;
    $apikey = 'YOUR-API-KEY';
    
    if($lang == "dk") {
      $submit_url="DK-LIST-URL";
    }
    elseif($lang == 'en') {
      $submit_url="EN-LIST-URL";
    }
    elseif($lang == 'es') {
      $submit_url="ES-LIST-URL";
    }
      
        $merge_vars = array(
            'FNAME' => $username,
            'USERNAME' => $username,
            'TYPE' => $type
        );
      
        $json = json_encode([
            'email_address' => $email,
            'merge_fields'  => $merge_vars,
            'status'        => 'subscribed'
        ]);
      
    $ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$submit_url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($ch,CURLOPT_POSTFIELDS,$json);
    $result=curl_exec($ch);
    curl_close($ch);
    $mcdata=json_decode($result);
      
    return $result;
        
    }
    
    function mailchimp_edit($email, $lang, $change) {
        
    $email = $email;
    $apikey = 'YOUR-API-KEY';
    
    if($lang == "dk") {
      $submit_url="DK-LIST-URL";
    }
    elseif($lang == 'en') {
      $submit_url="EN-LIST-URL";
    }
    elseif($lang == 'es') {
      $submit_url="ES-LIST-URL";
    }
      
      $submit_url = $submit_url . md5(strtolower($email));
      
        $json = json_encode([
            'status'        => $change
        ]);
      
    $ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$submit_url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apikey);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'PUT');
    curl_setopt($ch,CURLOPT_POSTFIELDS,$json);
    $result=curl_exec($ch);
    curl_close($ch);
    $mcdata=json_decode($result);
      
    return $result;
        
    }
    
}



